package services;

public class ConsolePrinterService implements PrinterService {

    public void print(String text) {
        System.out.println(text);
    }

}
