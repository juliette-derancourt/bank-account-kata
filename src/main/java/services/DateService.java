package services;

import java.time.Instant;

public interface DateService {

    Instant getDate();

}
