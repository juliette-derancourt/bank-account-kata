package services;

public interface PrinterService {

    void print(String text);

}
