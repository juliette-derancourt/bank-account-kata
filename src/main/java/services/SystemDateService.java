package services;

import java.time.Instant;

public class SystemDateService implements DateService {

    public Instant getDate() {
        return Instant.now();
    }

}
