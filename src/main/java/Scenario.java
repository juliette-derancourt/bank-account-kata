import bank.Account;
import bank.AccountHistory;
import services.ConsolePrinterService;
import services.SystemDateService;

import java.math.BigDecimal;
import java.time.Clock;

public class Scenario {

    public static void main(String[] args) {
        Account account = new Account(
                new AccountHistory(),
                new SystemDateService()
        );

        account.deposit(BigDecimal.valueOf(200));
        account.withdrawal(BigDecimal.valueOf(50));
        account.deposit(BigDecimal.valueOf(100));
        account.withdrawal(BigDecimal.valueOf(30));
        account.withdrawal(BigDecimal.valueOf(120));

        account.printStatement(Clock.systemDefaultZone().getZone(), new ConsolePrinterService());
    }

}
